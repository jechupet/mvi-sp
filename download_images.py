import os
import requests
import pandas as pd
import itertools

df1 = pd.read_csv("./url/american_flag_mountain.csv")
df2 = pd.read_csv("./url/american_flag_nature.csv")
df3 = pd.read_csv("./url/american_flag_buildings.csv")
df4 = pd.read_csv("./url/american_flag_demonstration.csv")
df5 = pd.read_csv("./url/american_flag_house.csv")
df6 = pd.read_csv("./url/USA_flag_army_parade.csv")

df_combined = pd.concat([df1, df2, df3, df4, df5, df6], ignore_index=True)
df_combined = df_combined.drop_duplicates()
df_combined = df_combined.reset_index(drop=True)
links = df_combined.values.tolist()


df_combined.info()

list_links = list(itertools.chain.from_iterable(links))

output_folder = "google_obrazky_AM_5"
os.makedirs(output_folder, exist_ok=True)

for idx, url in enumerate(list_links):
    if idx < 961:
        continue
    try:
        # Získání obsahu obrázku
        response = requests.get(url, stream=True)
        response.raise_for_status()  # Zkontroluj, jestli je požadavek úspěšný

        # Název souboru
        file_name = os.path.join(output_folder, f"image_{idx + 1}.jpg")

        # Uložení obrázku do souboru
        with open(file_name, "wb") as f:
            for chunk in response.iter_content(1024):
                f.write(chunk)
        print(f"Obrázek uložen: {file_name}")

    except Exception as e:
        print(f"Chyba při stahování {url}: {e}")
