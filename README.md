# Použití CycleGAN pro transformaci vlajek (České x USA) 


## Popis
Cílem tohoto projektu je vytvořit neuronovou síť typu CycleGAN, které dokáže transformovat obárzky (případně videa) obshaující Českou vlajku na obrázek obsahující vlajku USA a naopak. Za tímto účelem jsem nejprve musel vytvořit dva datasety (podrobnější popis později). Následně naprogramovat arichtekturu CycleGan, poté správně nastavit parametry, natrénovat samotnou síť a snažit se dosáhnout co nejlepších výsledků.  

## Data
Jak už bylo zmíněno, musel jsem vytvořit datasety pro každou skupinu. Moje prvotní myšlenka byla zkusit najít existující datasety profiltrovat je a použít. Bohužel jsem žádné takové data nenašel. Proto jsem šel metodou scrapování dat z internetu. Pro samotné scrapování obsahu jsem použil službu [Apify](https://apify.com/), tuto službu jsem použin několika způsoby. Nejprve pro scrapování obsahu z [Google images](https://apify.com/hooli/google-images-scraper), zde jsem používal ruzná query abych získal co největší rozmanitost. Zde je pá příkladů použitých query: 
<ul>
  <li>flag czech republic buildings</li>
  <li>flag czechia nature</li>
  <li>USA flag army parade</li>
  <li>american flag demonstration</li>
</ul>
Výsledkem této snahy byly url adresy obrázků, které jsem následně pomocí sciptu download_images.py stáhnul. Bohužel bylo potřeba tyto obrázky projít ručně a smazat výsledky, které vůbec neobsahovali vlajky. Tímto jsem získal přibližně 600 obrázků do každého datasetu. 

Následně jsem zkusil opět využít služby Apify, tentokráte pro sociální sítě a to konkrétně [instagram](https://apify.com/apify/instagram-hashtag-scraper) a [facebook](https://apify.com/apify/facebook-hashtag-scraper). Zde jsem hledal pomocí hashtagů (#czechflag, #americanflag, ...), opět získal url a provedl stejný proces co předtím. Bohužel se ukázalo, že ve veřejných částech (bez nutnosti přihlášení) těchto sociální síti je velmi málo vhodných obrázků. Celkově jsem touto cestou získal cca 100 obrázků do každé kategorie.

Jako poslední metodu pro získání dat jsem použil videa. Našel jsem vhodná videa obsahující vlajky, ty jsem pomocí scriptu "video_cutter.py" rozsekal po framech, a cyklicky vybíral obrázky (většinou po 5 nebo 10 framech). Toto jsem dělal, aby byl dataset maximálně různorody. Tímto způsobem jsem získal cca 600 obrázků do každého datasetu.

V adresáři "data_example", se můžete kouknout na to jak zhruba vypadají naše datasety.

## Rešerše
Jelikož jsem nikdy žádný GAN, natož CycleGAN nikdy nedělal, a ani neznal architekturu. Musel jsem si nejprve zjistit jak daná architektura funguje, k tomu mi pomohl tento [článek](https://junyanz.github.io/CycleGAN/), ve kterém je pěkná studie v této oblasti + příklady použití. Tento článek s připojeným repozitářem pro mě byla hlavní inspirace pro psaní výsledného kódu. Dále jsem ještě čerpal z této [stránky](https://medium.com/@chilldenaya/cyclegan-introduction-pytorch-implementation-5b53913741ca), na které jsou krásně dovysvětlené detaily a principy ztrátových funkcí. Pro následnou implementaci mi pak pomohlo toto [video](https://www.youtube.com/watch?v=4LktBHGCNfw&ab_channel=AladdinPersson), ze kterého jsem vycházel při tvoření kódu.
 

## Aktuální kód
Kód pro trénování a evaluaci lze nalézt na [zde](https://colab.research.google.com/drive/13Zq3Ehs1Th0YaiBvRiNlbW8YezH2szza?usp=sharing). V rámci trénování jsem využil prostředí Google colab, hlavně kvůli dostupnosti grafických karet. Tento kód je připojený k mému Google Drivu, na kterém jsou uložené datasety, dále tam verzuji váhy modelu a výsledky. Vybrané výsledky z trénování můžete vidět v adresáři result. 

Aktuálně jsem v procesu trénování, okolo 100 epochy. Doufám, že se výsledky ještě zlepší. Pokud ne, plánuji si pohrát s augmentací dat a případně ještě vylepšit datasety. Také si případně pohrát se ztrátovými funkcemi, což by mělo zlepšit výsledky.


