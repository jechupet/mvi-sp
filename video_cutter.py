import cv2
import os

# Cesta k videu
video_path = './videa/video_7.mp4'  # Nahraď cestou k tvému videu
output_folder = 'extrahovane_snimky_video7_CZE'    # Složka pro uložené snímky
frame_skip = 3  # Každý 5. snímek

# Vytvoř složku pro snímky, pokud ještě neexistuje
if not os.path.exists(output_folder):
    os.makedirs(output_folder)

# Načti video
cap = cv2.VideoCapture(video_path)

if not cap.isOpened():
    print("Nepodařilo se otevřít video.")
    exit()

frame_count = 0
saved_count = 0

while True:
    ret, frame = cap.read()
    if not ret:
        break

    # Pokud je aktuální snímek násobkem 5, ulož ho
    if frame_count % frame_skip == 0:
        filename = os.path.join(output_folder, f"frames_{frame_count}.jpg")
        cv2.imwrite(filename, frame)
        saved_count += 1

    frame_count += 1

cap.release()
print(f"Uloženo {saved_count} snímků do složky '{output_folder}'.")
